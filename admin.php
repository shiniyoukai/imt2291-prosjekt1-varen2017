<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prosjekt 1 - WWW-Teknologi</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/main.css">

  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php
        require_once 'include/topMenu.php';
        if ($user->isAdministrator()) {     // Only available if user is admin
            if (isset($_POST['addUser'])) { // Add a user
                $res = $user->addUser($_POST['new-email'], $_POST['new-password'], $_POST['new-firstname'], $_POST['new-lastname']);
                if (isset($res['success'])) { // Succesfully added ?>
                    <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                        <span class="sr-only">Bruker opprettet:</span>
                        Ny bruker er opprettet og kan ta i bruk systemet
                    </div> <?php
                } else { // Failed adding user ?>
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Feil:</span>
                        Kunne ikke opprette ny bruker, finnes e-post adressen registrert allerede?
                    </div> <?php
                }
            } else if (isset($_GET['deleteUser'])) { // remove a user
                $res = $user->removeUser($_GET['deleteUser']);
                if (isset($res['success'])) { // Successfully deleted ?>
                    <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                        <span class="sr-only">Bruker slettet:</span>
                        Brukeren er slettet fra systemet!
                    </div> <?php
                } else { // Failed deleting user ?>
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Feil:</span>
                        Kunne ikke slette brukeren!
                    </div> <?php
                }
            }
            ?>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Legg til en ny bruker</h3></div>
                    <div class="panel-body" style="margin-top: 10px;">
                        <form method="post" action="admin.php">
                            <div class="row">
                                <div class="col-xs-3">
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input type="email" class="form-control" name="new-email" placeholder="E-post adresse">
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" class="form-control" name="new-password" placeholder="Passord">
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" class="form-control" name="new-firstname" placeholder="Fornavn">
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" class="form-control" name="new-lastname" placeholder="Etternavn">
                                    </div>
                                </div>
                            </div>
                            <input type="submit" name="addUser" value="Legg til bruker" class="btn btn-primary"/>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Liste over brukere av systemet</h3></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="userTable" class="table table-striped table-hover">
                                <thead>
                                    <th>Username</th><th width="60%">Full name</th><th style="width:30px"></th>
                                </thead>
                                <tbody>
<?php
    $sql = "SELECT id, email, firstname, lastname from users ORDER BY email";
    $sth = $db->prepare ($sql);
    $sth->execute ();
    while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr>';
        echo "  <td><a href='mailto:{$row['email']}'>{$row['email']}</a></td><td>{$row['firstname']} {$row['lastname']}</td><td><a href='javascript: deleteUser({$row['id']}, ".'"'.$row['firstname']." ".$row['lastname'].'"'.");' title='Slett denne brukeren'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></td>";
        echo '</tr>';
    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="container">
                <div class="jumbotron">
                    <h1>You are not an administrator!</h1>
                    <p>Only administrators are allowed on this page</p>
                </div>
            </div>
        <?php } ?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script>
        $(function () { // Make the user table sortable, searchable and paginated
            $('#userTable').dynatable();
        });

        function deleteUser (id, name) {    // Avoid accidental removal of users
            var confirm = window.confirm('Er du sikker på at du vil slette brukeren ('+name+')');
            if (confirm)
                window.location = 'admin.php?deleteUser='+id;
        }
    </script>
  </body>
</html>
