<script>
$(function() {
		$('#uploadSuccess').hide();	// og upload success når siden vises
		$('#file').on('change', function (event) {	// Når brukeren har valgt en fil
			if (event.target.files.length==1) {		// Tillat kun en og en files
				$('#uploadFile').val(this.value);	// Vis filnavnet som er valgt
				var file = event.target.files[0];	// Litt kortere enn å referere til elementet i arrayen hver gang vil trenger informasjon om filen.
				$('#filename').val(file.name);
				$('#mime').val(file.type);
				var xhr = new XMLHttpRequest();		// Overfør filen via AJAX
				xhr.open("POST", 'include/receiveFile.php', true);		// Hvilket skript skal ta i mot filen
				xhr.setRequestHeader("X_FILENAME", file.name);	// Overfør informasjon om filnavnet
				xhr.setRequestHeader("X_MIMETYPE", file.type);	// Overfør informasjon om mime type
				xhr.setRequestHeader("X_FILESIZE", file.size);	// Overfør informasjon om filstørrelse
				xhr.upload.onprogress = function (event) {		// Motta beskjed underveis i overføringen
				}
				xhr.onload = function(e) {		// Kalles når overføringer er ferdig
					if (this.status == 200) {	// Dersom alt er OK
							var data = JSON.parse(this.responseText);
							if (data.ok) {
							$('#submitbutton').removeAttr("disabled");
							$('#filepath').val(data.filename);
							$('#uploadSuccess').hide();	// En liten fiks for fadeOut (nødvendig dersom en skal overføre flere filer)
							$('#uploadSuccess').show();	// Vis beskjed om suksess
							$('#uploadSuccess').fadeOut(4000);	// Fade ut denne
							// Her kan det også oppdateres en liste over filer som er overført.
						}
				}
				}
				xhr.send(file);		// Send filen
			}
		});
	});
	</script>
