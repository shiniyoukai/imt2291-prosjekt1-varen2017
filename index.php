<?php
session_start();

// Includes
require_once 'include/db.php';
require_once 'classes/user.php';

 ?>
<DOCTYPE html>
 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta name="description" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Prosjekt 1 - WWW-Teknologi</title>

     <link rel="apple-touch-icon" href="images/favicon.png">
     <link rel="icon" type="image/png" href="images/favicon.png">
     <!-- Place favicon.ico in the root directory -->

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
     <link rel="stylesheet" href="styles/main.css">
     <link rel="stylesheet" href="player/css/style.css" />
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

   </head>
   <body>

 <?php

 require_once 'include/topMenu.php'; // Includes navbar

 if (isset($_GET['video'])) {    // En video er valgt med via GET med navn video
             include_once ('include/video.php');    // Viser innholdet for avspillingssiden for video
         } else if ($user->isLoggedIn()) {   // Bruker er logget inn og blir sendt til sitt panel som viser egne videoer og spillelister
             include_once ('include/userPanel.php');
         } else {    // Besøkende får listet opp de nyeste videoene og spillelistene
             include_once ('include/newVideos.php');
         }


 ?>

 </body>
</html>
