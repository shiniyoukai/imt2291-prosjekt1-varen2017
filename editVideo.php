<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prosjekt 1 - WWW-Teknologi</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/main.css">
    <link rel="stylesheet" href="player/css/style.css" />
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php
        require_once 'include/topMenu.php';
        require_once 'classes/video.php';

        // Only allow editing of users own videos
        if (!$video->mine||!isset($_GET['video'])) { ?>
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Ingen tillatelse:</span>
                Du har ikke rettigheter til å redigere denne videoen eller valgt en ugyldig video!
            </div> <?php
        } else {
            $editing = true;
            // Creates the video tag with source and track info
            // The add to playlist option will also be created here
            include_once ('include/video.php');

            // Viser en form under videoen som gir mulighet for å laste opp track
            echo '<div class="container-fluid" style="margin-top: 10px"><div class="row">';
            echo '<div class="col-xs-12 col-lg-6 col-lg-offset-3">';
            $video->createUploadTrackForm();
            echo '</div>';
            echo '<div class="col-xs-12 col-lg-6">';
            echo '</div>';
            echo '</div></div>';

        }

    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </body>
</html>
