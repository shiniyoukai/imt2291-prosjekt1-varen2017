<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';

// Check if a user is logged in and a file has been uploaded
if ($user->isLoggedIn()&&isset($_POST['title'])) {

    // Add entry to database
    $sql = "INSERT INTO videos (owner_id, name, description, filename, mimetype, duration) VALUES (?, ?, ?, ?, ?, ?)";
    $sth = $db->prepare($sql);
    $sth->execute (array ($user->getUID(), $_POST['title'], $_POST['description'], $_POST['filename'], $_POST['mime'], ""));

    // Get id of last insertion, this will be part of the filename
    $id = $db->lastInsertId();

    // Move the file and give it a name where the id is part of the filename
    rename ($_POST['filepath'], "uploads/video_".$id);


    $uploadSuccess = true;
}

?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prosjekt1 - WWW-Teknologi</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/main.css">

  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php
        require_once 'include/topMenu.php';

        if (isset($uploadSuccess)) { // En video er lastet opp, gi brukeren beskjed ?>
            <div class="alert alert-success" role="alert">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <span class="sr-only">Suksess:</span>
                Videoen din er nå lastet opp.

            </div> <?php
        }

        if ($user->isLoggedIn()) {  // Only show file upload form if user is logger in
            require_once 'include/upload.html';
        }
        ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <?php

        if ($user->isLoggedIn()) {
            require_once 'scripts/upload.js';
        }

    ?>

  </body>
</html>
