<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Nye videoer</h3>
		        </div>
	            <div class="panel-body">
	            	<?php
	            		// Generates a list of videos
	            		require_once 'classes/video.php';
                  if (isset($_GET['query'])) {  // Show videos which contains the query value
                    $video->createVideoListTable("SELECT id, name, description FROM videos WHERE name LIKE ? OR description LIKE ? ORDER BY tstamp DESC ", array ('%'.$_GET['query'].'%', '%'.$_GET['query'].'%'));
                  } else {
                    $video->createVideoListTable("SELECT id, name, description FROM videos ORDER BY tstamp DESC LIMIT 10", array ());
                  }

	            	?>
	           	</div>
		    </div>
		</div>
		<div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Nye spillelister</h3>
		        </div>
	            <div class="panel-body">
		            <?php
			            // Generates a list of playlists
		                require_once ('classes/playlist.php');
                    if (isset($_GET['query'])) {  // Show playlist which contains the query value
                      $playlist->generatePlaylist ('SELECT id, name FROM playlists WHERE name LIKE ? ORDER BY tstamp DESC', array ('%'.$_GET['query'].'%'));
                    } else {
                      $playlist->generatePlaylist ('SELECT id, name FROM playlists ORDER BY tstamp DESC LIMIT 5', array ());
                    }

	                ?>
	           	</div>
		    </div>
		</div>
	</div>
</div>
