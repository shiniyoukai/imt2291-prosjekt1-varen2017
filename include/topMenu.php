<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php
        	if ($user->isLoggedIn()) {	// Upload menu item is only available for logged in users
        		echo '<li data-info="upload"><a href="upload.php">Last opp video</a></li>';
        	}
        	if ($user->isAdministrator()) { // Admin menu item is only available for admin
				echo '<li data-info="admin"><a href="admin.php">Admin</a></li>';
        	} ?>
      </ul>
      <form class="navbar-form navbar-left" action="" role="search">
        <div class="form-group">
          <input type="text" name="query" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li class="navbar-right" data-info="login"> <?php
        	if ($user->isLoggedIn()) {	// If logged in, show log out?>
        		<a href="logout.php?logout=true" title="logg ut">Logg ut</a></li> <?php
        	} else { // If not logged in, show log in ?>
	        	<a href="login.php">Logg inn</a></li> <?php
	        } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php echo $user->sessionAlert; // If a stolen session is suspected ?>
