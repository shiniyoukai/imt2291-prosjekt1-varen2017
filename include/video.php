<?php
// Video klasse for å kunne generere video-taggen
require_once 'classes/video.php';
?>
<div class="container-fluid">
	<div class="row" style="margin-right: 5px; margin-left: 5px">
		<div class="col-xs-12" id="videoDisplay" style="border: 1px solid lightgrey">

			<?php
			// Genererer video tag
			$video->createVideoTag($_GET['video']); ?>
		</div>
	</div>
</div>
